/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JOptionPane;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import modelo.Body;
import modelo.Validadores;
import org.orm.PersistentException;
import orm.*;

/**
 * @version 0.0.0,28/04/2017.
 * @author Eduard QF
 */
public class CreadorReportes {

    String xslh;
    /**
     * atributo que almacenara el objeto Body ya instanciado.
     */
    Body bd;

    /**
     * atributo que almacenara el contenido del reporte.
     */
    List<Object> reporte = new ArrayList<>();

    /**
     * atributo temporal que almacena contenido necesario del reporte.
     */
    List<Object> nodo1 = new ArrayList<>();

    /**
     * atributo que almacena contenido escrito necesario del reporte.
     */
    List<String> nodo2 = new ArrayList<>();

    /**
     * constructor de la Clase CreadorReportes.
     *
     * @param bd entrega la insatanciacion de Body creada desde un inicio.
     */
    public CreadorReportes(Body bd) {
        this.bd = bd;
    }

    /**
     * obtione los datos para generar un reporte dedicado a los apoderados.
     *
     *
     * @param cur curso del cual se generara el reporte.
     * @throws org.orm.PersistentException error
     */
    public void reporteApoderados(String cur) throws PersistentException {
        xslh = "Apoderados";
        String path = "C:\\InstitucionBasica\\Reportes\\reportes Obtenidos\\Reporte Padres Apoderados\\" + cur + "  reporte Apoderado-pupilos.xml";
        System.out.println("path: " + path);
        reporte.addAll(Arrays.asList(capanegocios.Read.readEstudiantesCurso(cur)));
        generador(reporte, path);
    }

    /**
     * metodo encargado de obtener los datos para el reporte de asignaturas de
     * los profesores.
     *
     * @param cur curso del que se generara el reporte.
     * @param asign asignatura del que se generara el reporte.
     * @throws org.orm.PersistentException error
     */
    public void reportePromedioNotasAsign(String cur, String asign) throws PersistentException {
        xslh = "Asignaturas";
        String path = "C:\\InstitucionBasica\\reportes Obtenidos\\Reportes Asignaturas Profesores\\" + cur + " Asignatura " + asign + ".xml";
        System.out.println("path: " + path);
        reporte.add("curso: " + cur);
        reporte.add("Asignatura: " + asign);
        Estudiante[] est = capanegocios.Read.readEstudiantesCurso(cur);
        for (Estudiante est1 : est) {
            List<Object> temp = new ArrayList<>();
            temp.add(est1);
            for (orm.Nota evaluacion : est1.nota.toArray()) {
                List<Object> temp2 = new ArrayList<>();
                if (bd.asignatura(asign, evaluacion.getActividad_id_fk1().getPlanificacion_id_fk().getAsignatura_id_fk().getNombreAsign())) {
                    temp2.add(evaluacion.getActividad_id_fk1().getDetalleAct());
                    temp2.add(String.valueOf(evaluacion.getNota()));
                }
                temp.add(temp2);
            }
            reporte.add(temp);
        }
        generador(reporte, path);
    }

    /**
     * metodo encargado de obtener los datos para el reporte Asistencia y notas
     * de los alumnos.
     *
     * @param cur curso para el que se realizara el reporte.
     * @throws org.orm.PersistentException error
     */
    public void reportePromedioAsistenciaNotas(String cur) throws PersistentException {
        xslh = "AsisNot";
        String path = "C:\\Users\\Eduard QF\\Desktop\\inf\\Programacio2017\\reportes Obtenidos\\Reporte Asistencias Notas\\" + cur + " Asistencia y Notas.xml";
        System.out.println("path: " + path);
        nodo2.add("curso: " + cur);
        for (Estudiante alumn : capanegocios.Read.readEstudiantesCurso(cur)) {
            List<Object> rep = new ArrayList<>();
            rep.add(alumn.getPersona_id_fk().getNombre());
            rep.add("Asistencia: " + String.valueOf(alumn.getAsistenciaFinal()) + "%");
            rep.addAll(Arrays.asList(alumn.nota.toArray()));
            reporte.add(rep);
        }
        generador(reporte, path);
    }

    /**
     * metodo encargado de obtener los datos para el reporte de asistencias
     * menor a un limite indicado.
     *
     * @param cur curso de que se realizara el reporte.
     * @param pAsis porsentaje de asistencia requerida.
     * @throws org.orm.PersistentException error
     */
    public void reporteAsisX(String cur, int pAsis) throws PersistentException {
        xslh = "Asis";
        String path = "C:\\Users\\Eduard QF\\Desktop\\inf\\Programacio2017\\reportes Obtenidos\\Reportes Asistecia x\\" + cur + " Asistencia bajo " + pAsis + " porciento.xml";
        System.out.println("path: " + path);
        for (Estudiante est : capanegocios.Read.readEstudiantesCurso(cur)/*int i = 0; i < in.getCursos().get(cur).getAlumnos().size(); i++*/) {
            List<Object> temp = new ArrayList<>();
            if (Validadores.porsentaje(est.getAsistenciaFinal(), pAsis)) {
                temp.add(est.getPersona_id_fk().getNombre());
                temp.add(String.valueOf(est.getAsistenciaFinal()) + "%");
            }
            if (!temp.isEmpty()) {
                reporte.add(temp);
            }
        }
        generador(reporte, path);
    }

    /**
     * metodo encargado de obtener los datos para el reporte de los alumnos
     * reprobados por cursos.
     *
     * @param cur curso al que se realizara el reporte.
     * @throws org.orm.PersistentException error
     */
    public void reporteReprobadosCursos(String cur) throws PersistentException {
        xslh = "Rep";
        String path = "C:\\InstitucionBasica\\reportes Obtenidos\\Reporte Reprobados curso\\" + cur + " Reprobados.xml";
        System.out.println("path: " + path);
        reporte.add("curso: " + cur);
        for (Estudiante alumn : capanegocios.Read.readEstudiantesCurso(cur)) {
            List<Object> temp = new ArrayList<>();
            if (Validadores.reprobado(alumn)) {
                temp.add(alumn.getPersona_id_fk().getNombre());
                temp.add(String.valueOf(alumn.getPromedioFinal()));
                temp.add(alumn.getAsistenciaFinal() + "%");
            }
            reporte.add(temp);
        }
        generador(reporte, path);
    }

    /**
     * metodo encargado de obtener los datos para el reporte de los apoderados
     * que tienen multiples pupilos en la institucion.
     *
     * @throws org.orm.PersistentException error
     */
    public void reporteMultiplesPupilos() throws PersistentException {
        xslh = "AMP";
        String path = "C:\\InstitucionBasica\\reportes Obtenidos\\Reporte apoderados multiple pupilos\\ Apoderados con multiples pupilos.xml";
        System.out.println("path: " + path);
        llenarReporteP();
        generador(nodo1, path);
    }

    /**
     * llena los datos para el reporte de Apoderados con multiples pupilos en el
     * establecimiento.
     */
    private void llenarReporteP() throws PersistentException {
        ArrayList<Object> apoderado = new ArrayList<>();
        for (Apoderado ap : capanegocios.Read.readComplitApoderado()) {
            if ((ap.estudiante.toArray().length) > 1) {
                apoderado.add(ap);
            }
        }

    }

    /**
     * entrega el reporte a cada apoderado de la planificacion de sus pupilos
     *
     * @param apoderado apoderado al que se le generara la planificacion.
     * @throws org.orm.PersistentException error
     */
    public void reportePlanificacion(String apoderado) throws PersistentException {
        xslh = "Apoder";
        String path = "C:\\InstitucionBasica\\reportes Obtenidos\\Reportes Planificaciones\\Planificacion pupilos.xml";
        System.out.println("path: " + path);

        reporte.add(apoderado);
        for (Estudiante pupilo : capanegocios.Read.readPupilos(apoderado)) {
            System.out.println("pupilo:" + pupilo.getPersona_id_fk().getNombre());
            reporte.add(pupilo.getPersona_id_fk());
            ArrayList<Planificacion> planificacion = new ArrayList<>();
            for (Asignatura asign : pupilo.getCurso_id_fk().asignatura.toArray()) {
                planificacion.add(asign.getPlanificacion());
            }
            reporte.add(planificacion);
        }
        generador(reporte, path);

    }

    /**
     * metodo encargado de generar un xml de los datos obtenidos.
     */
    private void generador(List report, String path) {
        XStream xs = new XStream();
        xs.alias("Reporte", report.getClass());
        String t = xs.toXML(report);
        datos.DataReader.writeData(path, t);
        System.out.println("reporte generado");
        Gson g = new Gson();
        String p = g.toJson(report);
        datos.DataReader.writeData(path.replaceAll("xml", "json"), p);
        System.out.println("json creado");
        nodo2.clear();
        nodo1.clear();
        reporte.clear();
        transformador(path);
        JOptionPane.showMessageDialog(null, "reporte creado exitosamente");
    }

    /**
     * metodo que transforma el xml creado en el generador y crea verciones con
     * formato html,json,xls y doc.
     *
     * @param path indica cual es la direccion del archivo xml a transformar.
     */
    private void transformador(String path) {
        Source xmlDoc = new StreamSource(path);
        String root = "";
        String name = "";
        for (int i = 0; i < 3; i++) {
            System.out.println("transformador i:" + i);
            try {
                TransformerFactory tFactory = TransformerFactory.newInstance();
                switch (i) {
                    case 0://html
                        root = "C:\\Users\\Eduard QF\\Documents\\NetBeansProjects\\ProgramacionCursosBasica\\src\\XSLs\\To_Html_" + xslh + ".xsl";
                        name = path.replaceAll("xml", "html");
                        break;
                    case 1://xls(excel)
                        root = "C:\\Users\\Eduard QF\\Documents\\NetBeansProjects\\ProgramacionCursosBasica\\src\\XSLs\\To_Exel.xsl";
                        name = path.replaceAll("xml", "xls");
                        break;
                    case 2://doc(word)
                        root = "C:\\Users\\Eduard QF\\Documents\\NetBeansProjects\\ProgramacionCursosBasica\\src\\XSLs\\To_Word.xsl";
                        name = path.replaceAll("xml", "doc");
                        break;
                }
                Source xslDoc = new StreamSource(root);
                String outputFileName = name;

                OutputStream htmlFile = new FileOutputStream(outputFileName);
                Transformer trasform = tFactory.newTransformer(xslDoc);
                trasform.transform(xmlDoc, new StreamResult(htmlFile));

            } catch (FileNotFoundException | TransformerFactoryConfigurationError | TransformerConfigurationException e) {
                e.getMessage();
                e.getLocalizedMessage();
            } catch (TransformerException e) {
                e.getMessageAndLocation();
            }
        }

    }
}
