/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Asistencia {
	public Asistencia() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_ASISTENCIA_ESTUDIANTE_ID_FK) {
			this.estudiante_id_fk = (orm.Estudiante) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Estudiante estudiante_id_fk;
	
	private Boolean asis;
	
	private String fecha;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * indica la fecha en que se paso la asistencia
     * @param value fecha que se anoto la asistencia
	 */
	public void setFecha(String value) {
		this.fecha = value;
	}
	
	/**
	 * indica la fecha en que se paso la asistencia
     * @return fecha de la asistencia
	 */
	public String getFecha() {
		return fecha;
	}
	
	/**
	 * indica la asistencia del alumno en esa fecha
     * @param value valor de la asistencia
	 */
	public void setAsis(boolean value) {
		setAsis(new Boolean(value));
	}
	
	/**
	 * indica la asistencia del alumno en esa fecha
     * @param value valor de la asistencia
	 */
	public void setAsis(Boolean value) {
		this.asis = value;
	}
	
	/**
	 * indica la asistencia del alumno en esa fecha
     * @return la asistencia de esa fecha
	 */
	public Boolean getAsis() {
		return asis;
	}
	
	public void setEstudiante_id_fk(orm.Estudiante value) {
		if (estudiante_id_fk != null) {
			estudiante_id_fk.asistencia.remove(this);
		}
		if (value != null) {
			value.asistencia.add(this);
		}
	}
	
	public orm.Estudiante getEstudiante_id_fk() {
		return estudiante_id_fk;
	}
	
	/**
	 * This method is for internal use only.
     * @param value estudinate al que se le registra la anotacion
	 */
	public void setORM_Estudiante_id_fk(orm.Estudiante value) {
		this.estudiante_id_fk = value;
	}
	
	private orm.Estudiante getORM_Estudiante_id_fk() {
		return estudiante_id_fk;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
