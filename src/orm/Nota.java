/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Nota {

    public Nota() {
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_NOTA_ESTUDIANTE_ID_FK) {
            this.estudiante_id_fk = (orm.Estudiante) owner;
        } else if (key == orm.ORMConstants.KEY_NOTA_ACTIVIDAD_ID_FK1) {
            this.actividad_id_fk1 = (orm.Actividad) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private orm.Estudiante estudiante_id_fk;

    private Float nota;

    private orm.Actividad actividad_id_fk1;

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    /**
     * indica la nota obtenida
     *
     * @param value valor de la nota
     */
    public void setNota(float value) {
        setNota(new Float(value));
    }

    /**
     * indica la nota obtenida
     *
     * @param value valor de la nota
     */
    public void setNota(Float value) {
        this.nota = value;
    }

    /**
     * indica la nota obtenida
     *
     * @return el valor de la nota
     */
    public Float getNota() {
        return nota;
    }

    public void setEstudiante_id_fk(orm.Estudiante value) {
        if (estudiante_id_fk != null) {
            estudiante_id_fk.nota.remove(this);
        }
        if (value != null) {
            value.nota.add(this);
        }
    }

    public orm.Estudiante getEstudiante_id_fk() {
        return estudiante_id_fk;
    }

    /**
     * This method is for internal use only.
     *
     * @param value estudiante al que pertenecera la nota
     */
    public void setORM_Estudiante_id_fk(orm.Estudiante value) {
        this.estudiante_id_fk = value;
    }

    private orm.Estudiante getORM_Estudiante_id_fk() {
        return estudiante_id_fk;
    }

    public void setActividad_id_fk1(orm.Actividad value) {
        if (actividad_id_fk1 != null) {
            actividad_id_fk1.nota1.remove(this);
        }
        if (value != null) {
            value.nota1.add(this);
        }
    }

    public orm.Actividad getActividad_id_fk1() {
        return actividad_id_fk1;
    }

    /**
     * This method is for internal use only.
     *
     * @param value actividad a la que pertenecera la nota
     */
    public void setORM_Actividad_id_fk1(orm.Actividad value) {
        this.actividad_id_fk1 = value;
    }

    private orm.Actividad getORM_Actividad_id_fk1() {
        return actividad_id_fk1;
    }

    public String toString() {
        return String.valueOf(getId());
    }

}
