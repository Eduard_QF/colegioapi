/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AsignaturaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id_;
	public final IntegerExpression curso_id_fkId;
	public final AssociationExpression curso_id_fk;
	public final IntegerExpression profesor_id_fkId;
	public final AssociationExpression profesor_id_fk;
	public final StringExpression nombreAsign;
	public final IntegerExpression planificacionId;
	public final AssociationExpression planificacion;
	
	public AsignaturaCriteria(Criteria criteria) {
		super(criteria);
		id_ = new IntegerExpression("id_", this);
		curso_id_fkId = new IntegerExpression("curso_id_fk.id", this);
		curso_id_fk = new AssociationExpression("curso_id_fk", this);
		profesor_id_fkId = new IntegerExpression("profesor_id_fk.id", this);
		profesor_id_fk = new AssociationExpression("profesor_id_fk", this);
		nombreAsign = new StringExpression("nombreAsign", this);
		planificacionId = new IntegerExpression("planificacion.id_", this);
		planificacion = new AssociationExpression("planificacion", this);
	}
	
	public AsignaturaCriteria(PersistentSession session) {
		this(session.createCriteria(Asignatura.class));
	}
	
	public AsignaturaCriteria() throws PersistentException {
		this(orm.LibroClasePersistentManager.instance().getSession());
	}
	
	public CursoCriteria createCurso_id_fkCriteria() {
		return new CursoCriteria(createCriteria("curso_id_fk"));
	}
	
	public ProfesorCriteria createProfesor_id_fkCriteria() {
		return new ProfesorCriteria(createCriteria("profesor_id_fk"));
	}
	
	public PlanificacionCriteria createPlanificacionCriteria() {
		return new PlanificacionCriteria(createCriteria("planificacion"));
	}
	
	public Asignatura uniqueAsignatura() {
		return (Asignatura) super.uniqueResult();
	}
	
	public Asignatura[] listAsignatura() {
		java.util.List list = super.list();
		return (Asignatura[]) list.toArray(new Asignatura[list.size()]);
	}
}

