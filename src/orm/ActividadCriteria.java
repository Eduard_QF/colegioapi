/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ActividadCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression planificacion_id_fkId;
	public final AssociationExpression planificacion_id_fk;
	public final StringExpression detalleAct;
	public final StringExpression fecha;
	public final CollectionExpression nota1;
	
	public ActividadCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		planificacion_id_fkId = new IntegerExpression("planificacion_id_fk.id", this);
		planificacion_id_fk = new AssociationExpression("planificacion_id_fk", this);
		detalleAct = new StringExpression("detalleAct", this);
		fecha = new StringExpression("fecha", this);
		nota1 = new CollectionExpression("ORM_Nota1", this);
	}
	
	public ActividadCriteria(PersistentSession session) {
		this(session.createCriteria(Actividad.class));
	}
	
	public ActividadCriteria() throws PersistentException {
		this(orm.LibroClasePersistentManager.instance().getSession());
	}
	
	public PlanificacionCriteria createPlanificacion_id_fkCriteria() {
		return new PlanificacionCriteria(createCriteria("planificacion_id_fk"));
	}
	
	public NotaCriteria createNota1Criteria() {
		return new NotaCriteria(createCriteria("ORM_Nota1"));
	}
	
	public Actividad uniqueActividad() {
		return (Actividad) super.uniqueResult();
	}
	
	public Actividad[] listActividad() {
		java.util.List list = super.list();
		return (Actividad[]) list.toArray(new Actividad[list.size()]);
	}
}

