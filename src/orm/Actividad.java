/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 *
 * This is an automatic generated file. It will be regenerated every time
 * you generate persistence class.
 *
 * Modifying its content may cause the program not work, or your work may lost.
 */
/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Actividad {

    public Actividad() {
    }

    private java.util.Set this_getSet(int key) {
        if (key == orm.ORMConstants.KEY_ACTIVIDAD_NOTA1) {
            return ORM_nota1;
        }

        return null;
    }

    private void this_setOwner(Object owner, int key) {
        if (key == orm.ORMConstants.KEY_ACTIVIDAD_PLANIFICACION_ID_FK) {
            this.planificacion_id_fk = (orm.Planificacion) owner;
        }
    }

    org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
        public java.util.Set getSet(int key) {
            return this_getSet(key);
        }

        public void setOwner(Object owner, int key) {
            this_setOwner(owner, key);
        }

    };

    private int id;

    private orm.Planificacion planificacion_id_fk;

    private String detalleAct;

    private String fecha;

    private java.util.Set ORM_nota1 = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public int getORMID() {
        return getId();
    }

    /**
     * detalle de la actividad
     *
     * @param value valor del detalle
     */
    public void setDetalleAct(String value) {
        this.detalleAct = value;
    }

    /**
     * detalle de la actividad
     * @return el detalle de la actividad
     */
    public String getDetalleAct() {
        return detalleAct;
    }

    /**
     * fecha en que se realiara la actividad
     * @param value valor de la fecha
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * fecha en que se realiara la actividad
     * @return  retorna el valor de la fecha
     */
    public String getFecha() {
        return fecha;
    }

    public void setPlanificacion_id_fk(orm.Planificacion value) {
        if (planificacion_id_fk != null) {
            planificacion_id_fk.actividad.remove(this);
        }
        if (value != null) {
            value.actividad.add(this);
        }
    }

    public orm.Planificacion getPlanificacion_id_fk() {
        return planificacion_id_fk;
    }

    /**
     * This method is for internal use only.
     * @param value valor de la planificacion
     */
    public void setORM_Planificacion_id_fk(orm.Planificacion value) {
        this.planificacion_id_fk = value;
    }

    private orm.Planificacion getORM_Planificacion_id_fk() {
        return planificacion_id_fk;
    }

    private void setORM_Nota1(java.util.Set value) {
        this.ORM_nota1 = value;
    }

    private java.util.Set getORM_Nota1() {
        return ORM_nota1;
    }

    public final orm.NotaSetCollection nota1 = new orm.NotaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_ACTIVIDAD_NOTA1, orm.ORMConstants.KEY_NOTA_ACTIVIDAD_ID_FK1, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);

    public String toString() {
        return String.valueOf(getId());
    }

}
