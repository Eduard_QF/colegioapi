/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ActividadDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression planificacion_id_fkId;
	public final AssociationExpression planificacion_id_fk;
	public final StringExpression detalleAct;
	public final StringExpression fecha;
	public final CollectionExpression nota1;
	
	public ActividadDetachedCriteria() {
		super(orm.Actividad.class, orm.ActividadCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		planificacion_id_fkId = new IntegerExpression("planificacion_id_fk.id", this.getDetachedCriteria());
		planificacion_id_fk = new AssociationExpression("planificacion_id_fk", this.getDetachedCriteria());
		detalleAct = new StringExpression("detalleAct", this.getDetachedCriteria());
		fecha = new StringExpression("fecha", this.getDetachedCriteria());
		nota1 = new CollectionExpression("ORM_Nota1", this.getDetachedCriteria());
	}
	
	public ActividadDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.ActividadCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		planificacion_id_fkId = new IntegerExpression("planificacion_id_fk.id", this.getDetachedCriteria());
		planificacion_id_fk = new AssociationExpression("planificacion_id_fk", this.getDetachedCriteria());
		detalleAct = new StringExpression("detalleAct", this.getDetachedCriteria());
		fecha = new StringExpression("fecha", this.getDetachedCriteria());
		nota1 = new CollectionExpression("ORM_Nota1", this.getDetachedCriteria());
	}
	
	public PlanificacionDetachedCriteria createPlanificacion_id_fkCriteria() {
		return new PlanificacionDetachedCriteria(createCriteria("planificacion_id_fk"));
	}
	
	public NotaDetachedCriteria createNota1Criteria() {
		return new NotaDetachedCriteria(createCriteria("ORM_Nota1"));
	}
	
	public Actividad uniqueActividad(PersistentSession session) {
		return (Actividad) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Actividad[] listActividad(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Actividad[]) list.toArray(new Actividad[list.size()]);
	}
}

