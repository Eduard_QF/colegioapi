/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Asignatura {
	public Asignatura() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_ASIGNATURA_CURSO_ID_FK) {
			this.curso_id_fk = (orm.Curso) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_ASIGNATURA_PROFESOR_ID_FK) {
			this.profesor_id_fk = (orm.Profesor) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_ASIGNATURA_PLANIFICACION) {
			this.planificacion = (orm.Planificacion) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id_;
	
	private orm.Curso curso_id_fk;
	
	private orm.Profesor profesor_id_fk;
	
	private String nombreAsign;
	
	private orm.Planificacion planificacion;
	
	private void setId_(int value) {
		this.id_ = value;
	}
	
	public int getId_() {
		return id_;
	}
	
	public int getORMID() {
		return getId_();
	}
	
	/**
	 * nombre de la asignatura inscrita.
     * @param value nombre de la asignatura
	 */
	public void setNombreAsign(String value) {
		this.nombreAsign = value;
	}
	
	/**
	 * nombre de la asignatura inscrita.
     * @return nombre de la asignatura
	 */
	public String getNombreAsign() {
		return nombreAsign;
	}
	
	public void setCurso_id_fk(orm.Curso value) {
		if (curso_id_fk != null) {
			curso_id_fk.asignatura.remove(this);
		}
		if (value != null) {
			value.asignatura.add(this);
		}
	}
	
	public orm.Curso getCurso_id_fk() {
		return curso_id_fk;
	}
	
	/**
	 * This method is for internal use only.
     * @param value curso al que pertenece la asignatura
	 */
	public void setORM_Curso_id_fk(orm.Curso value) {
		this.curso_id_fk = value;
	}
	
	private orm.Curso getORM_Curso_id_fk() {
		return curso_id_fk;
	}
	
	public void setProfesor_id_fk(orm.Profesor value) {
		if (profesor_id_fk != null) {
			profesor_id_fk.asignatura.remove(this);
		}
		if (value != null) {
			value.asignatura.add(this);
		}
	}
	
	public orm.Profesor getProfesor_id_fk() {
		return profesor_id_fk;
	}
	
	/**
	 * This method is for internal use only.
     * @param value profesor que imparte la asignatura
	 */
	public void setORM_Profesor_id_fk(orm.Profesor value) {
		this.profesor_id_fk = value;
	}
	
	private orm.Profesor getORM_Profesor_id_fk() {
		return profesor_id_fk;
	}
	
	public void setPlanificacion(orm.Planificacion value) {
		if (this.planificacion != value) {
			orm.Planificacion lplanificacion = this.planificacion;
			this.planificacion = value;
			if (value != null) {
				planificacion.setAsignatura_id_fk(this);
			}
			if (lplanificacion != null && lplanificacion.getAsignatura_id_fk() == this) {
				lplanificacion.setAsignatura_id_fk(null);
			}
		}
	}
	
	public orm.Planificacion getPlanificacion() {
		return planificacion;
	}
	
	public String toString() {
		return String.valueOf(getId_());
	}
	
}
