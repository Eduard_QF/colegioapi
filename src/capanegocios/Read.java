package capanegocios;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.orm.PersistentException;
import orm.*;

/**
 *
 * @author Eduard QF
 */
public class Read {

    /**
     * metodo que debuelve los curso en una institucion en especifico.
     *
     * @return un vector de cursos pertenecientes a la institucion.
     *
     */
    public static orm.Curso[] readCursos() {
        try {
            orm.Institucion loarmInstitucion;

            loarmInstitucion = orm.InstitucionDAO.loadInstitucionByQuery("id_pk = 1", "id_pk");
            return loarmInstitucion.curso.toArray("idCurso", true);
        } catch (PersistentException ex) {
            Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * metodo que debuelve un curso en especifico.
     *
     * @param idCurso identificador del curso
     * @return el curso cargado desde la base de datos, en caso contrario
     * debuelve nulo.
     *
     *
     */
    public static orm.Curso readCurso(String idCurso) {
        orm.Curso loarmCurso;
        if (idCurso != null) {
            try {
                loarmCurso = orm.CursoDAO.loadCursoByQuery("idCurso = '" + idCurso + "'", "idCurso");
                return loarmCurso;
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que debuelve las asignaturas de un curso en especifico.
     *
     * @param idCurso identificador del curso al que pertenecen las asignaturas.
     * @return un vector que contiene las asignaturas pertenecientes al curso
     * indicado.
     *
     */
    public static orm.Asignatura[] readAsignaturas(String idCurso) {
        orm.Curso loarmCurso;
        if (idCurso != null) {
            try {
                loarmCurso = orm.CursoDAO.loadCursoByQuery("idCurso = '" + idCurso + "'", "idCurso");
                return loarmCurso.asignatura.toArray();
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que debuelve una asignatura especifica de un curso en especifico.
     *
     * @param nombreAsign identificador de la asignatura.
     * @param cursoid identificador del curso al que pertenece la asignatura
     * (id_fk).
     * @return la asignatura cargada, si los paramentros no son nulos.
     *
     */
    public static orm.Asignatura readAsignatura(String nombreAsign, String cursoid) {
        orm.Asignatura loarmAsignatura;
        if (nombreAsign != null && cursoid != null) {
            try {
                Curso loarmCurso = readCurso(cursoid);
                loarmAsignatura = orm.AsignaturaDAO.loadAsignaturaByQuery("nombreAsign = '" + nombreAsign + "' AND  curso_id_fk = '" + loarmCurso.getId() + "'", "curso_id_fk");
                return loarmAsignatura;
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metood que debuelve un arreglo de estudiantes pertenecientes a un curso.
     *
     * @param idCurso indica el curso al que pertenecen los estudiantes.
     * @return un vector que contiene los estudiantes del curso, si los
     * paramentros no son nulos.
     *
     */
    public static orm.Estudiante[] readEstudiantesCurso(String idCurso) {
        orm.Curso loarmCurso;
        if (idCurso != null) {
            try {
                loarmCurso = orm.CursoDAO.loadCursoByQuery("idCurso = '" + idCurso + "'", "idCurso");
                System.out.println("return true");
                return loarmCurso.estudiante.toArray();
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("return false");
        return null;
    }

    /**
     * metodo que retorna un vector de pupilos de un apoderado especifico.
     *
     * @param nomOrut nombre o rut del apoderado que se busca.
     * @return un vector que contiene los estudiantes, si los paramentros no son
     * nulos.
     *
     */
    public static orm.Estudiante[] readPupilos(String nomOrut) {
        orm.Persona loarmPersona;
        orm.Apoderado loarmApoderado;
        if (modelo.Validadores.validarFormatoRut(nomOrut)) {
            try {
                loarmPersona = orm.PersonaDAO.loadPersonaByQuery("rut = '" + nomOrut + "'", "rut");
                loarmApoderado = orm.ApoderadoDAO.loadApoderadoByQuery("persona_id_fk = " + loarmPersona.getId(), "persona_id_fk = ");
                return loarmApoderado.estudiante.toArray();
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (modelo.Validadores.validarFormatoNombre(nomOrut)) {
            try {
                loarmPersona = orm.PersonaDAO.loadPersonaByQuery("nombre ='" + nomOrut + "'", "nombre");
                loarmApoderado = orm.ApoderadoDAO.loadApoderadoByQuery("persona_id_fk = " + loarmPersona.getId(), "persona_id_fk = ");
                return loarmApoderado.estudiante.toArray();
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que debuelve un estudiante en especifico, para el rut ingresado.
     *
     * @param nomOrut rut perteneciente al estudiante que se busca.
     * @return un estudiante en especifico, si los paramentros no son nulos.
     *
     */
    public static orm.Estudiante readEstudiante(String nomOrut) {
        orm.Persona per;
        orm.Estudiante loarmEstudiante;
        if (modelo.Validadores.validarFormatoRut(nomOrut)) {
            try {
                System.out.println("busqueda por rut.");
                per = orm.PersonaDAO.loadPersonaByQuery("rut = '" + nomOrut + "'", "rut");
                loarmEstudiante = orm.EstudianteDAO.loadEstudianteByQuery("persona_id_fk =" + per.getId(), "id_pk");
                return loarmEstudiante;
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                System.out.println("busqueda por nombre.");
                per = orm.PersonaDAO.loadPersonaByQuery("nombre = '" + nomOrut + "'", "nombre");
                loarmEstudiante = orm.EstudianteDAO.loadEstudianteByQuery("persona_id_fk =" + per.getId(), "id_pk");
                return loarmEstudiante;
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;

    }

    /**
     * metodo que debuelve las notas de un estudinte en especifico.
     *
     * @param rut rut perteneciente al estudiante que se busca.
     * @return un vector con las notas correspondientes al alumno, si los
     * paramentros no son nulos.
     *
     */
    public static orm.Nota[] readNotasAlumno(String rut) {
        orm.Persona per;
        orm.Estudiante loarmEstudiante;
        if (rut != null) {
            try {
                per = orm.PersonaDAO.loadPersonaByQuery("rut = '" + rut + "'", "rut");
                loarmEstudiante = orm.EstudianteDAO.loadEstudianteByQuery("persona_id_fk =" + per.getId(), "id_pk");
                return loarmEstudiante.nota.toArray();
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que debuelve las notas de una asignatura.
     *
     * @param curso curso al que pertenecela asignatura.
     * @param asignatura asignaturas del que se leeran las notas.
     *
     * @return un vector con las notas correspondientes al alumno, si los
     * paramentros no son nulos.
     *
     */
    public static orm.Nota[] readNotasAsignatura(String curso, String asignatura) {
        Curso cur = readCurso(curso);
        Actividad activ[] = readAsignatura(asignatura, curso).getPlanificacion().actividad.toArray();
        Nota[] notas = null;
        if (activ != null) {
            for (Actividad act : activ) {
                notas = act.nota1.toArray();
            }
        }

        return notas;
    }

    /**
     * metodo que retorna una nota en especifico de un estudiante.
     *
     * @param alumno estudiante al que pertenece la nota.
     * @param act actividad en la cual se evualo con la nota.
     * @return una nota especifica en caso de que exista, en caso contrario
     * debuelve nulo, si los paramentros no son nulos.
     *
     */
    public static orm.Nota readNota(orm.Estudiante alumno, orm.Actividad act) {
        orm.Nota loarmNota;
        if (alumno != null && act != null) {
            try {
                loarmNota = orm.NotaDAO.loadNotaByQuery("estudiante_id_fk = '" + alumno.getId() + "' AND actividad_id_fk1 = '" + act.getId() + "'", "estudiante_id_fk");
                if (loarmNota != null) {
                    return loarmNota;
                }
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que retorna una nota en especifico de un estudiante.
     *
     * @param alumno estudiante al que pertenece la nota.
     * @param act actividad en la cual se evualo con la nota.
     * @return una nota especifica en caso de que exista, en caso contrario
     * debuelve nulo, si los paramentros no son nulos.
     *
     */
    public static orm.Nota readNota(orm.Estudiante alumno, String act) {
        orm.Nota loarmNota;
        if (alumno != null && act != null) {
            try {
                loarmNota = orm.NotaDAO.loadNotaByQuery("estudiante_id_fk = '" + alumno.getId() + "' AND actividad_id_fk1 = '" + act + "'", "estudiante_id_fk");
                return loarmNota;
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que debuelve un arreglo de las asistencia de un estudiante.
     *
     * @param rut rut del estudiante al que pertenecen los estudiantes.
     * @return un vector que contiene las asistencias del estudiante, si los
     * paramentros no son nulos.
     *
     */
    public static orm.Asistencia[] readAsistencias(String rut) {
        orm.Persona per;
        orm.Estudiante loarmEstudiante;
        if (rut != null) {
            try {
                per = orm.PersonaDAO.loadPersonaByQuery("rut = '" + rut + "'", "rut");
                loarmEstudiante = orm.EstudianteDAO.loadEstudianteByQuery("persona_id_fk =" + per.getId(), "id_pk");
                return loarmEstudiante.asistencia.toArray();
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que debuelve una asistencia especifica de un alumno.
     *
     * @param fecha fecha en que se realizo la asistencia.
     * @param alumno estudiante al que le pertenece
     * @return una asistencia especifica para el alumno indicado, si los
     * paramentros no son nulos.
     *
     */
    public static orm.Asistencia readAsistencia(String fecha, orm.Estudiante alumno) {
        orm.Asistencia loarmAsistencia;
        if (fecha != null && alumno != null) {
            try {
                loarmAsistencia = orm.AsistenciaDAO.loadAsistenciaByQuery("fecha =" + fecha + "' And ' estudiante_id_fk =" + alumno.getId(), "estudiante_id_fk");
                return loarmAsistencia;
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que debuelve las anotaciones de un alumno.
     *
     * @param rut rut del alumno al que se obtendran las notas.
     * @return un vector que contiene todas las anotaciondes del alumno, si los
     * paramentros no son nulos.
     *
     */
    public static orm.Anotaciones[] readAnotaciones(String rut) {
        orm.Persona per;
        orm.Estudiante loarmEstudiante;
        if (rut != null) {
            try {
                per = orm.PersonaDAO.loadPersonaByQuery("rut = '" + rut + "'", "rut");
                loarmEstudiante = orm.EstudianteDAO.loadEstudianteByQuery("persona_id_fk = '" + per.getId() + "'", "id_pk");
                return loarmEstudiante.anotaciones.toArray();
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que debuelve una anptacion especifica de un alumno.
     *
     * @param fecha fecha en que fue creada la anotacion.
     * @param alumno estudiante al que pertenece la anotacion.
     * @return una anotacion en especifica del estudiante, si los paramentros no
     * son nulos.
     *
     */
    public static orm.Anotaciones readAnotacion(String fecha, String alumno) {
        orm.Anotaciones loarmAnotacion;
        if (fecha != null && alumno != null) {
            try {
                Estudiante loarmEstudiante = readEstudiante(alumno);
                loarmAnotacion = orm.AnotacionesDAO.loadAnotacionesByQuery("estudiante_id_fk = " + loarmEstudiante.getId() + "' And 'fecha" + fecha + "'", fecha);
                return loarmAnotacion;
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que debuelve la planificacion de una asignatura en especifica.
     *
     * @param asign asignatura a la que pertenece la planificacion.
     * @param curso curso al que pertenece la planificacion.
     * @return una planificacion en especifica, si los paramentros no son nulos.
     *
     */
    public static orm.Planificacion readPlanificacion(orm.Asignatura asign, orm.Curso curso) {
        orm.Planificacion loarmPlanificacion;
        if (asign != null && curso != null) {
            try {
                loarmPlanificacion = orm.PlanificacionDAO.loadPlanificacionByQuery("asignatura_id_fk = " + asign.getId_(), "id_pk");
                return loarmPlanificacion;
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que debuelve las actividades de una asignatura.
     *
     * @param nombre nombre de la asignatura que se busca.
     * @param curso curso al que pertenece la asignatura.
     * @return un vector que contiene las actividades de la asignatura, si los
     * paramentros no son nulos.
     *
     */
    public static orm.Actividad[] readActividades(String nombre, String curso) {
        orm.Planificacion loarmPlanificacion;
        if (nombre != null && curso != null) {
            try {
                Asignatura loarmAsign = readAsignatura(nombre, curso);
                loarmPlanificacion = orm.PlanificacionDAO.loadPlanificacionByQuery("asignatura_id_fk = " + loarmAsign.getId_(), "id_pk");
                return loarmPlanificacion.actividad.toArray();
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que debuelve las actividades de una asignatura.
     *
     * @param asig asignatura que se busca.
     * @return un vector que contiene las actividades de la asignatura, si los
     * paramentros no son nulos.
     *
     */
    public static orm.Actividad[] readActividades(orm.Asignatura asig) {
        if (asig != null) {
            return asig.getPlanificacion().actividad.toArray();
        }
        return null;
    }

    /**
     * metodo que debuelve una actividad en especifica de una asignatura.
     *
     * @param fecha fecha de la actividad.
     * @param plan planificacion a la que pertenece la actividad.
     * @return una activiad en especifica, si los parametros no son nulos.
     *
     */
    public static orm.Actividad readActividad(String fecha, orm.Planificacion plan) {
        orm.Actividad loarmActividad;
        if (fecha != null && plan != null) {
            try {
                loarmActividad = orm.ActividadDAO.loadActividadByQuery("fecha = " + fecha + " ' AND ' planificaion_id_fk = " + plan.getId(), "");
                return loarmActividad;
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que debuelve los preofesores de un curso.
     *
     * @param idCurso identificador del curso al que pertenecen los profesores.
     * @return un vector con los profesores pertenecientes al curso.
     *
     */
    public static orm.Profesor[] readProfesoresCurso(String idCurso) {
        ArrayList<Profesor> profesores = new ArrayList<>();
        orm.Curso loarmCurso;
        if (idCurso != null) {
            try {
                loarmCurso = orm.CursoDAO.loadCursoByQuery("idCurso = '" + idCurso + "'", "idCurso");
                for (orm.Asignatura asign : loarmCurso.asignatura.toArray()) {
                    profesores.add(asign.getProfesor_id_fk());
                }
                Profesor[] profes = new Profesor[profesores.size()];

                for (int i = 0; i < profesores.size(); i++) {
                    profes[i] = profesores.get(i);
                }
                return profes;
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo encargado de leer todos los profesores encontrads en la base de
     * datos.
     *
     * @return un arreglo que contiene todos los profesores.
     */
    public static orm.Profesor[] readProfesoresInstitucion() {
        try {
            return ProfesorDAO.listProfesorByQuery(null, null);
        } catch (PersistentException ex) {
            Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * metodo encargado de leer todos los apoderados encontrads en la base de
     * datos.
     *
     * @return un arreglo que contiene todos los apoderados.
     */
    public static orm.Apoderado[] readApoderadosInstitucion() {
        try {
            return ApoderadoDAO.listApoderadoByQuery(null, null);
        } catch (PersistentException ex) {
            Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * metodo que debuelve un profesor en especifico.
     *
     * @param nomOrut rut o nombre de profesor que se busca.
     * @return un profesor en especifico, si los paramentros no son nulos.
     *
     */
    public static orm.Profesor readProfesor(String nomOrut) {
        orm.Persona per;
        orm.Profesor loarmProfesor;
        if (modelo.Validadores.validarFormatoRut(nomOrut)) {
            try {
                per = orm.PersonaDAO.loadPersonaByQuery("rut = '" + nomOrut + "'", "rut");
                loarmProfesor = orm.ProfesorDAO.loadProfesorByQuery("persona_id_fk = " + per.getId(), "persona_id_fk");
                return loarmProfesor;
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (modelo.Validadores.validarFormatoNombre(nomOrut)) {
            try {
                per = orm.PersonaDAO.loadPersonaByQuery("nombre = '" + nomOrut + "'", "nombre");
                loarmProfesor = orm.ProfesorDAO.loadProfesorByQuery("persona_id_fk = " + per.getId(), "persona_id_fk");
                return loarmProfesor;
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que retorna todos los apoderados de los estudiantes de un curso.
     *
     * @param idCurso indicador del curso.
     * @return un vector con todos los profesores del curso.
     *
     */
    public static orm.Apoderado[] readApoderadosCurso(String idCurso) {
        ArrayList<orm.Apoderado> apoderados = new ArrayList<>();
        orm.Curso loarmCurso;
        if (idCurso != null) {
            try {
                loarmCurso = orm.CursoDAO.loadCursoByQuery("idCurso = '" + idCurso + "'", "idCurso");
                for (orm.Estudiante estudiante : loarmCurso.estudiante.toArray()) {
                    apoderados.add(estudiante.getApoderado_id_fk());
                }
                orm.Apoderado[] ap = new Apoderado[apoderados.size()];
                for (int i = 0; i < ap.length; i++) {
                    ap[i] = apoderados.get(i);
                }
                return ap;
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que retorna un apoderado en especifico.
     *
     * @param rut rut del apoderado que se busca.
     * @return un apoderado en especifico , si los paramentros no son nulos.
     *
     */
    public static orm.Apoderado readApoderado(String rut) {
        orm.Persona per;
        orm.Apoderado loarmApoderado;
        if (rut != null) {
            try {
                per = orm.PersonaDAO.loadPersonaByQuery("rut ='" + rut + "'", "rut");
                loarmApoderado = orm.ApoderadoDAO.loadApoderadoByQuery("persona_id_fk = " + per.getId(), "persona_id_fk");
                return loarmApoderado;
            } catch (PersistentException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * metodo que retorna un apoderado en especifico.
     *
     * @return un apoderado en especifico , si los paramentros no son nulos.
     *
     */
    public static orm.Apoderado[] readComplitApoderado() {
        try {
            orm.Apoderado[] loarmApoderado;
            loarmApoderado = orm.ApoderadoDAO.listApoderadoByQuery(null, null);
            return loarmApoderado;
        } catch (PersistentException ex) {
            Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
