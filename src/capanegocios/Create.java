/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capanegocios;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import orm.*;

/**
 *
 * @author Eduard QF
 */
public class Create {

    /**
     * metodo que crea una nueva institucion.
     *
     * @param nombreInstitucion nombre que tendra la institucion.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createInstitucion(String nombreInstitucion) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Institucion loarmInstitucion = InstitucionDAO.createInstitucion();
                loarmInstitucion.setNombre(nombreInstitucion);
                InstitucionDAO.save(loarmInstitucion);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea una nueva institucion.
     *
     * @param nombreInstitucion nombre que tendra la institucion.
     * @param cursos cursos que tendra la institucion.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createInstitucion(String nombreInstitucion, Curso[] cursos) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Institucion loarmInstitucion = InstitucionDAO.createInstitucion();
                loarmInstitucion.setNombre(nombreInstitucion);
                for (Curso curso : cursos) {
                    loarmInstitucion.curso.add(curso);
                }
                InstitucionDAO.save(loarmInstitucion);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea un nuevo curso
     *
     * @param idCurso indentificador del curso.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createCurso(String idCurso) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Institucion institucion = InstitucionDAO.loadInstitucionByQuery("id_pk= 1 ", "id_pk");
                Curso loarmcurso = CursoDAO.createCurso();
                loarmcurso.setIdCurso(idCurso);
                loarmcurso.setInstitucion_id_fk(institucion);
                CursoDAO.save(loarmcurso);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }
        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea un nuevo curso
     *
     * @param idCurso indentificador del curso.
     * @param institucion institucion a la que pertenece.
     * @param alumno estudiantes que pertenecen al curso.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createCurso(String idCurso, Institucion institucion, Estudiante[] alumno) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Curso loarmcurso = CursoDAO.createCurso();
                loarmcurso.setIdCurso(idCurso);
                loarmcurso.setInstitucion_id_fk(institucion);
                for (Estudiante est : alumno) {
                    loarmcurso.estudiante.add(est);
                }
                CursoDAO.save(loarmcurso);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea un nuevo curso
     *
     * @param idCurso indentificador del curso.
     * @param institucion institucion a la que pertenece.
     * @param asign asignaturas pertenecientes al curso.
     * @return estado de si la creacion se realizao correctamente.
     */
    public static boolean createCurso(String idCurso, Institucion institucion, Asignatura[] asign) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Curso loarmcurso = CursoDAO.createCurso();
                loarmcurso.setIdCurso(idCurso);
                loarmcurso.setInstitucion_id_fk(institucion);
                for (Asignatura est : asign) {
                    loarmcurso.asignatura.add(est);
                }
                CursoDAO.save(loarmcurso);
                t.commit();
                return true;
            } catch (PersistentException e) {
                try {
                    t.rollback();
                    return false;
                } catch (PersistentException ex) {
                    Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return false;

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea un nuevo curso
     *
     * @param idCurso indentificador del curso.
     * @param institucion institucion a la que pertenece.
     * @param alumno estudiantes que pertenecen al curso.
     * @param asign asignaturas pertenecientes al curso.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createCurso(String idCurso, Institucion institucion, Estudiante[] alumno, Asignatura[] asign) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Curso loarmcurso = CursoDAO.createCurso();
                loarmcurso.setIdCurso(idCurso);
                loarmcurso.setInstitucion_id_fk(institucion);
                for (Asignatura est : asign) {
                    loarmcurso.asignatura.add(est);
                }
                for (Asignatura est : asign) {
                    loarmcurso.asignatura.add(est);
                }
                CursoDAO.save(loarmcurso);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea una asignatura.
     *
     * @param nombre identificador de la asignatura.
     * @param cur curso al que pertenece.
     * @param profesor profesor encargado de impartirla.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createAsignatura(String nombre, Curso cur, Profesor profesor) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Asignatura loarmAsignatura = AsignaturaDAO.createAsignatura();
                loarmAsignatura.setNombreAsign(nombre);
                loarmAsignatura.setCurso_id_fk(cur);
                loarmAsignatura.setProfesor_id_fk(profesor);
                AsignaturaDAO.save(loarmAsignatura);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea un estudiante.
     *
     * @param nombre nombre del estudiante.
     * @param rut rut del estudiante.
     * @param cur curso al que pertenece el aumno.
     * @param ap apoderado del alumno.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createEstudiante(String nombre, String rut, Curso cur, Apoderado ap) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Estudiante loarmEstudiante = EstudianteDAO.createEstudiante();
                loarmEstudiante.setPersona_id_fk(createPersona(nombre, rut, loarmEstudiante));
                loarmEstudiante.setORM_Curso_id_fk(cur);
                loarmEstudiante.setApoderado_id_fk(ap);
                ap.estudiante.add(loarmEstudiante);
                EstudianteDAO.save(loarmEstudiante);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea un estudiante.
     *
     * @param nombre nombre del estudiante.
     * @param rut rut del estudiante.
     * @param cur curso al que pertenece el aumno.
     * @param nombreAP nombre del apoderado.
     * @param rutAP rut del apoderado.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createEstudiante(String nombre, String rut, Curso cur, String nombreAP, String rutAP) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Estudiante loarmEstudiante = EstudianteDAO.createEstudiante();
                loarmEstudiante.setPersona_id_fk(createPersona(nombre, rut, loarmEstudiante));
                loarmEstudiante.setORM_Curso_id_fk(cur);
                Apoderado ap = createApoderado1(nombreAP, rutAP, loarmEstudiante);
                loarmEstudiante.setApoderado_id_fk(ap);
                ap.estudiante.add(loarmEstudiante);
                EstudianteDAO.save(loarmEstudiante);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea un apoderado.
     *
     * @param nombre nombre del estudiante.
     * @param rut rut del apoderado.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createApoderado(String nombre, String rut) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Apoderado loarmApoderado = ApoderadoDAO.createApoderado();
                loarmApoderado.setPersona_id_fk(createPersona(nombre, rut, loarmApoderado));
                ApoderadoDAO.save(loarmApoderado);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea un apoderado.
     *
     * @param nombre nombre del estudiante.
     * @param rut rut del apoderado.
     * @param alumno pupilo del apoderado.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createApoderado(String nombre, String rut, Estudiante alumno) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Apoderado loarmApoderado = ApoderadoDAO.createApoderado();
                loarmApoderado.setPersona_id_fk(createPersona(nombre, rut, loarmApoderado));
                loarmApoderado.estudiante.add(alumno);
                ApoderadoDAO.save(loarmApoderado);
                t.commit();
                return true;
            } catch (PersistentException e) {
                try {
                    t.rollback();
                    return false;
                } catch (PersistentException ex) {
                    Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return false;

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea un apoderado.
     *
     * @param nombre nombre del estudiante.
     * @param rut rut del apoderado.
     * @param alumno pupilo del apoderado.
     * @return estado de si la creacion se realizao correctamente.
     * @throws org.PersistentException
     */
    private static Apoderado createApoderado1(String nombre, String rut, Estudiante alumno) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Apoderado loarmApoderado = ApoderadoDAO.createApoderado();
                loarmApoderado.setPersona_id_fk(createPersona(nombre, rut, loarmApoderado));
                loarmApoderado.estudiante.add(alumno);
                ApoderadoDAO.save(loarmApoderado);
                t.commit();
                return loarmApoderado;
            } catch (PersistentException e) {
                t.rollback();
                return null;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    /**
     * metodo que crea un profesor.
     *
     * @param nombre nombre que tendra el profesor.
     * @param rut rut que tendra el profesor.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createProfesor(String nombre, String rut) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Profesor loarmProfesor = ProfesorDAO.createProfesor();
                loarmProfesor.setPersona_id_fk(createPersona(nombre, rut, loarmProfesor));
                ProfesorDAO.save(loarmProfesor);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea un profesor.
     *
     * @param nombre nombre que tendra el profesor.
     * @param rut rut que tendra el profesor.
     * @param asigns asignaturas que impartira el profesor.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createProfesor(String nombre, String rut, Asignatura[] asigns) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Profesor loarmProfesor = ProfesorDAO.createProfesor();
                loarmProfesor.setPersona_id_fk(createPersona(nombre, rut, loarmProfesor));
                for (Asignatura asign : asigns) {
                    loarmProfesor.asignatura.add(asign);
                }
                ProfesorDAO.save(loarmProfesor);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea un profesor.
     *
     * @param nombre nombre que tendra el profesor.
     * @param rut rut que tendra el profesor.
     * @param asigns asignatura que impartira el profesor.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createProfesor(String nombre, String rut, Asignatura asigns) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Profesor loarmProfesor = ProfesorDAO.createProfesor();
                loarmProfesor.setPersona_id_fk(createPersona(nombre, rut, loarmProfesor));
                loarmProfesor.asignatura.add(asigns);
                ProfesorDAO.save(loarmProfesor);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea una planificacion.
     *
     * @param asign asignatura a la que pertenecera la planificacion.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createPlanificacion(Asignatura asign) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Planificacion loarmPlanificacion = PlanificacionDAO.createPlanificacion();
                loarmPlanificacion.setAsignatura_id_fk(asign);
                PlanificacionDAO.save(loarmPlanificacion);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea una planificacion.
     *
     * @param asign asignatura a la que pertenecera la planificacion.
     * @param actividades actividades que componen la planificacion.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createPlanificacion(Asignatura asign, Actividad[] actividades) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Planificacion loarmPlanificacion = PlanificacionDAO.createPlanificacion();
                loarmPlanificacion.setAsignatura_id_fk(asign);
                for (Actividad act : actividades) {
                    loarmPlanificacion.actividad.add(act);
                }
                PlanificacionDAO.save(loarmPlanificacion);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea una actividad.
     *
     * @param detalle detalle de la actividad.
     * @param fecha fecha a realizarse la actividad.
     * @param plan planificacion a la que pertenese la actividad.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createActividad(String detalle, String fecha, Planificacion plan) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();

            Actividad loarmActividad = ActividadDAO.createActividad();
            loarmActividad.setDetalleAct(detalle);
            loarmActividad.setFecha(fecha);
            loarmActividad.setPlanificacion_id_fk(plan);
            ActividadDAO.save(loarmActividad);
            t.commit();
            return true;

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea una asistencia.
     *
     * @param presencia indica si el estudiante estuvo precente(true) o
     * ausente(false).
     * @param fecha fecha en que se paso la asistencia.
     * @param alumno estudiante al que pertenece la asistencia.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createAsistencia(boolean presencia, String fecha, Estudiante alumno) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Asistencia loarmAsistencia = AsistenciaDAO.createAsistencia();
                loarmAsistencia.setAsis(presencia);
                loarmAsistencia.setFecha(fecha);
                loarmAsistencia.setEstudiante_id_fk(alumno);
                AsistenciaDAO.save(loarmAsistencia);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea una anotacion.
     *
     * @param detalle datos de la anotacion.
     * @param fecha fecha en que se realizo la anotacion.
     * @param alumno estudiante al que pertenece la anotacion.
     * @param tipoAnotacion indica el estado de la anotación positiva(true),
     * negativa(false);
     * @param docente profesor que escribio la anotacion.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createAnotacion(String detalle, String fecha, boolean tipoAnotacion, Estudiante alumno, Profesor docente) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Anotaciones loarmAnotacion = AnotacionesDAO.createAnotaciones();
                loarmAnotacion.setDetalle(detalle);
                loarmAnotacion.setFecha(fecha);
                loarmAnotacion.setTipo(tipoAnotacion);
                loarmAnotacion.setEstudiante_id_fk(alumno);
                loarmAnotacion.setProfesor_id_fk(docente);
                AnotacionesDAO.save(loarmAnotacion);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea una nota.
     *
     * @param nota evaluacion que obtuvo el estudiante.
     * @param act actividad a la que pertenece la nota.
     * @param alumno estudiante al que pertenece la nota.
     * @return estado de si la creacion se realizao correctamente.
     *
     */
    public static boolean createNota(float nota, Actividad act, Estudiante alumno) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Nota loarmNota = NotaDAO.createNota();
                loarmNota.setNota(nota);
                loarmNota.setActividad_id_fk1(act);
                loarmNota.setEstudiante_id_fk(alumno);
                NotaDAO.save(loarmNota);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que crea una persona.
     *
     * @param nombre nombre que tendra la persona.
     * @param rut rut que tendra la persona.
     * @return la persona.
     *
     */
    public static Persona createPersona(String nombre, String rut) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Persona loarmPersona = PersonaDAO.createPersona();
                loarmPersona.setNombre(nombre);
                loarmPersona.setRut(rut);
                PersonaDAO.save(loarmPersona);

                t.commit();
                return loarmPersona;
            } catch (PersistentException e) {
                t.rollback();
                return null;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    /**
     * metodo que crea una persona.
     *
     * @param nombre nombre que tendra la persona.
     * @param rut rut que tendra la persona.
     * @param ap apoderado del que es propiedad la persona.
     * @return la persona.
     *
     */
    public static Persona createPersona(String nombre, String rut, Apoderado ap) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Persona loarmPersona = PersonaDAO.createPersona();
                loarmPersona.setNombre(nombre);
                loarmPersona.setRut(rut);
                loarmPersona.setApoderado(ap);
                PersonaDAO.save(loarmPersona);
                t.commit();
                return loarmPersona;
            } catch (PersistentException e) {
                t.rollback();
                return null;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    /**
     * metodo que crea una persona.
     *
     * @param nombre nombre que tendra la persona.
     * @param rut rut que tendra la persona.
     * @param es estudiante del que es propiedad la persona.
     * @return la persona.
     *
     */
    public static Persona createPersona(String nombre, String rut, Estudiante es) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Persona loarmPersona = PersonaDAO.createPersona();
                loarmPersona.setNombre(nombre);
                loarmPersona.setRut(rut);
                loarmPersona.setEstudiante(es);
                PersonaDAO.save(loarmPersona);
                t.commit();
                return loarmPersona;
            } catch (PersistentException e) {
                t.rollback();
                return null;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    /**
     * metodo que crea una persona.
     *
     * @param nombre nombre que tendra la persona.
     * @param rut rut que tendra la persona.
     * @param docente profesor del que es propiedad la persona.
     * @return la persona.
     *
     */
    public static Persona createPersona(String nombre, String rut, Profesor docente) {
        try {
            PersistentTransaction t = LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                Persona loarmPersona = PersonaDAO.createPersona();
                loarmPersona.setNombre(nombre);
                loarmPersona.setRut(rut);
                loarmPersona.setProfesor(docente);
                PersonaDAO.save(loarmPersona);
                t.commit();
                return loarmPersona;
            } catch (PersistentException e) {
                t.rollback();
                return null;
            }

        } catch (PersistentException ex) {
            Logger.getLogger(Create.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
}
