package capanegocios;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

/**
 * @version 0.0.1,23/05/2017
 * @author Eduard QF
 */
public class Delete {

    /**
     * metodo que elimina una institucion.
     *
     * @param nombreInstitucion nombre de la institucion a ser eliminada.
     * @return true si la institucion fue borrada exitosamente.

     */
    public static boolean deleteInstitucion(){
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Institucion loarmInstitucion = orm.InstitucionDAO.loadInstitucionByQuery("id_pk = 1", "id_pk");
                System.out.println("nombre institucion: "+loarmInstitucion.getNombre());
                orm.InstitucionDAO.delete(loarmInstitucion);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }
            
        } catch (PersistentException ex) {
            Logger.getLogger(Delete.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que elimina un curso.@return true si la institucion fue borrada
     * exitosamente.
     *
     * @param idCurso identificador del curso a ser eliminado.
     * @param institucion identificador de la institucion a la que pertenece el
     * curso.
     * @return true si el curso fue borrada exitosamente.

     */
    public static boolean deleteCurso(String idCurso, orm.Institucion institucion){
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Curso loarmCurso = orm.CursoDAO.loadCursoByQuery("idCurso = " + idCurso + "' AND ' institucion_id_fk = " + institucion.getId() + "'", "idCurso");
                orm.CursoDAO.delete(loarmCurso);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }
            
        } catch (PersistentException ex) {
            Logger.getLogger(Delete.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metood que elimina una asignatura.
     *
     * @param nombre identificador de la asignatura a ser eliminada.
     * @param cur identificador del curso alq que pertenece la asignatura.
     * @return true si la asignatura fue borrada exitosamente.

     */
    public static boolean deleteAsignatura(String nombre, orm.Curso cur){
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Asignatura loarmAsignatura = orm.AsignaturaDAO.loadAsignaturaByQuery("'nombreAsign = " + nombre + "' AND ' curso_id_fk = " + cur.getId(), "curso_id_fk");
                deletePlanificacion(loarmAsignatura);
                loarmAsignatura.getProfesor_id_fk().asignatura.remove(loarmAsignatura);
                orm.AsignaturaDAO.delete(loarmAsignatura);
                t.commit();
                return true;
            } catch (PersistentException e) {
                try {
                    t.rollback();
                    return false;
                } catch (PersistentException ex) {
                    Logger.getLogger(Delete.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return false;
            
        } catch (PersistentException ex) {
            Logger.getLogger(Delete.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que elimina un estudiante.
     *
     * @param nombre nombre de estudiante a ser eliminado.
     * @param rut rut del estudiante a ser eliminado.
     * @return true si el estudiante fue borrado exitosamente.

     */
    public static boolean deleteEstudiante(String nombre, String rut){
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Persona per = orm.PersonaDAO.loadPersonaByQuery("rut = " + rut, "rut");
                orm.Estudiante loarmEstudiante = orm.EstudianteDAO.loadEstudianteByQuery("persona_id_fk =" + per.getId(), "id_pk");
                loarmEstudiante.getApoderado_id_fk().estudiante.remove(loarmEstudiante);
                
                for (orm.Nota nota : loarmEstudiante.nota.toArray()) {
                    deleteNota(nota.getActividad_id_fk1(), loarmEstudiante);
                }
                
                for (orm.Asistencia asis : loarmEstudiante.asistencia.toArray()) {
                    deleteAsistencia(asis.getFecha(), loarmEstudiante);
                }
                
                for (orm.Anotaciones an : loarmEstudiante.anotaciones.toArray()) {
                    deleteAnotacion(an.getFecha(), loarmEstudiante);
                }
                orm.EstudianteDAO.delete(loarmEstudiante);
                deletePersona(nombre, rut);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }
            
        } catch (PersistentException ex) {
            Logger.getLogger(Delete.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que elimina una apoderado.
     *
     * @param nombre nombre del apoderado a ser eliminado
     * @param rut rut del a poderado a ser eliminado.
     * @return true si el apoderado fue borrado exitosamente.

     */
    public static boolean deleteApoderado(String nombre, String rut){
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Persona per = orm.PersonaDAO.loadPersonaByQuery("rut = " + rut, "rut");
                orm.Apoderado loarmApoderado = orm.ApoderadoDAO.loadApoderadoByQuery("persona_id_fk = " + per.getId(), "persona_id_fk");
                deletePersona(nombre, rut);
                for (orm.Estudiante pupilo : loarmApoderado.estudiante.toArray()) {
                    deleteEstudiante(pupilo.getPersona_id_fk().getNombre(), pupilo.getPersona_id_fk().getRut());
                }
                orm.ApoderadoDAO.delete(loarmApoderado);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                System.out.println(e);
                return false;
            }
            
        } catch (PersistentException ex) {
            Logger.getLogger(Delete.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que elimina un profesor.
     *
     * @param nombre nombre del profesor a ser eliminado
     * @param rut rut del profesor que a ser eliminado.
     * @return true si el profesor fue borrado exitosamente.

     */
    public static boolean deleteProfesor(String nombre, String rut){
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Persona per = orm.PersonaDAO.loadPersonaByQuery("rut = " + rut, "rut");
                orm.Profesor loarmProfesor = orm.ProfesorDAO.loadProfesorByQuery("persona_id_fk = " + per.getId(), "per_id_fk");
                deletePersona(nombre, rut);
                for (orm.Asignatura asig : loarmProfesor.asignatura.toArray()) {
                    deleteAsignatura(asig.getNombreAsign(), asig.getCurso_id_fk());
                }
                orm.ProfesorDAO.delete(loarmProfesor);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }
            
        } catch (PersistentException ex) {
            Logger.getLogger(Delete.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que elimina una planificacion.
     *
     * @param asign identificador de la asignatura a la que pertenece
     * @return true si la planificacion fue borrada exitosamente.

     */
    public static boolean deletePlanificacion(orm.Asignatura asign){
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Planificacion loarmPlanificacion = orm.PlanificacionDAO.loadPlanificacionByQuery("asignatura_id_fk = " + asign.getId_(), "id_pk");
                for (orm.Actividad act : loarmPlanificacion.actividad.toArray()) {
                    deleteActividad(act.getFecha(), loarmPlanificacion);
                }
                loarmPlanificacion.getAsignatura_id_fk().setPlanificacion(null);
                orm.PlanificacionDAO.delete(loarmPlanificacion);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }
            
        } catch (PersistentException ex) {
            Logger.getLogger(Delete.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que elimina una actividad.
     *
     * @param fecha fecha en la que esta estipulada a la actividad.
     * @param plan indicador de la planificacion a la que pertenece la
     * actividad.
     * @return true si la actividad fue borrada exitosamente.

     */
    public static boolean deleteActividad(String fecha, orm.Planificacion plan){
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Actividad loarmActividad = orm.ActividadDAO.loadActividadByQuery("fecha = " + fecha + " ' AND ' planificaion_id_fk = " + plan.getId(), "");
                loarmActividad.getPlanificacion_id_fk().actividad.remove(loarmActividad);
                loarmActividad.nota1.clear();
                orm.ActividadDAO.delete(loarmActividad);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }
            
        } catch (PersistentException ex) {
            Logger.getLogger(Delete.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que elimina una asistencia.
     *
     * @param fecha fecha a la que pertenece la asistencia.
     * @param alumno indicador del alumno al que pertenece la asistencia.
     * @return true si la asistencia fue borrada exitosamente.

     */
    public static boolean deleteAsistencia(String fecha, orm.Estudiante alumno){
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Asistencia loarmAsistencia = orm.AsistenciaDAO.loadAsistenciaByQuery("fecha =" + fecha + "' And ' estudiante_id_fk =" + alumno.getId(), "estudiante_id_fk");
                loarmAsistencia.getEstudiante_id_fk().asistencia.remove(loarmAsistencia);
                orm.AsistenciaDAO.delete(loarmAsistencia);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }
            
        } catch (PersistentException ex) {
            Logger.getLogger(Delete.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que elimina un anotacion.
     *
     * @param fecha fecha en que se coloco la anotacion.
     * @param alumno alumno al que pertenece la anotacion.
     * @return true si la anotacion fue borrada exitosamente.

     */
    public static boolean deleteAnotacion(String fecha, orm.Estudiante alumno){
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Anotaciones loarmAnotacion = orm.AnotacionesDAO.loadAnotacionesByQuery("estudiante_id_fk = " + alumno.getId() + "' And 'fecha" + fecha + "'", fecha);
                orm.AnotacionesDAO.delete(loarmAnotacion);
                loarmAnotacion.getProfesor_id_fk().anotaciones.remove(loarmAnotacion);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }
            
        } catch (PersistentException ex) {
            Logger.getLogger(Delete.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que elimina una nota.
     *
     * @param act actividad a la que pertenece la nota.
     * @param alumno alumno al que pertenece la nota.
     * @return true si la nota fue borrada exitosamente.

     */
    public static boolean deleteNota(orm.Actividad act, orm.Estudiante alumno){
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Nota loarmNota = orm.NotaDAO.loadNotaByQuery("estudiante_id_fk" + alumno.getId() + "' AND 'actividad_id_fk1" + act.getPlanificacion_id_fk() + "'", "estudiante_id_fk");
                loarmNota.getEstudiante_id_fk().nota.remove(loarmNota);
                orm.NotaDAO.delete(loarmNota);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }
            
        } catch (PersistentException ex) {
            Logger.getLogger(Delete.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * metodo que elimina una persona.
     *
     * @param nombre nombre de la persona a ser eliminada
     * @param rut rut de la persona a ser eliminada
     * @return true si la persona fue borrada exitosamente.
     */
    public static boolean deletePersona(String nombre, String rut){
        try {
            PersistentTransaction t = orm.LibroClasePersistentManager.instance().getSession().beginTransaction();
            try {
                orm.Persona loarmPersona = orm.PersonaDAO.loadPersonaByQuery("nombre =" + nombre + "' AND ' rut =" + rut + "'", "rut");
                orm.PersonaDAO.delete(loarmPersona);
                t.commit();
                return true;
            } catch (PersistentException e) {
                t.rollback();
                return false;
            }
        } catch (PersistentException ex) {
            Logger.getLogger(Delete.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
