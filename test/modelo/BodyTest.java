/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import orm.Institucion;
import orm.Profesor;

/**
 *
 * @author Eduard QF
 */
public class BodyTest {
    
    public BodyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of init method, of class Body.
     * @throws java.lang.Exception
     */
    @Test
    public void testInit() throws Exception {
        System.out.println("init");
        Body instance = new Body();
        instance.init();
        
        
    }

    /**
     * Test of asignInst method, of class Body.
     */
    @Test
    public void testAsignInst() {
        System.out.println("asignInst");
        Body instance = new Body();
        Institucion expResult = null;
        Institucion result = instance.asignInst();
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of compareDocentes method, of class Body.
     */
    
    public void testCompareDocentes() {
        System.out.println("compareDocentes");
        Profesor[] profesores = null;
        String nombre = "";
        Body instance = new Body();
        Profesor expResult = null;
        Profesor result = instance.compareDocentes(profesores, nombre);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of guardarCambios method, of class Body.
     * @throws java.lang.Exception
     */
    @Test
    public void testGuardarCambios() throws Exception {
        System.out.println("guardarCambios");
        Body instance = new Body();
        instance.guardarCambios();
        
        
    }

    /**
     * Test of getFecha method, of class Body.
     */
    @Test
    public void testGetFecha() {
        System.out.println("getFecha");
        Body instance = new Body();
         Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String expResult = format.format(fecha);
        String result = instance.getFecha();
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of comparadorString method, of class Body.
     */
    @Test
    public void testComparadorString() {
        System.out.println("comparadorString");
        String text = "";
        String string = "";
        Body instance = new Body();
        boolean expResult = true;
        boolean result = instance.comparadorString(text, string);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of getIdcurso method, of class Body.
     */
    @Test
    public void testGetIdcurso() {
        System.out.println("getIdcurso");
        int i = 0;
        Body instance = new Body();
        String expResult = "1 A";
        String result = instance.getIdcurso(i);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of getAsignatura method, of class Body.
     */
    @Test
    public void testGetAsignatura() {
        System.out.println("getAsignatura");
        int i = 0;
        Body instance = new Body();
        String expResult = "Lenguaje";
        String result = instance.getAsignatura(i);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of asignatura method, of class Body.
     */
    @Test
    public void testAsignatura() {
        System.out.println("asignatura");
        String materia = "Matematica";
        String asign = "Lenguaje";
        Body instance = new Body();
        boolean expResult = false;
        boolean result = instance.asignatura(materia, asign);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of llenarCurso method, of class Body.
     * @throws java.lang.Exception
     */
    @Test
    public void testLlenarCurso() throws Exception {
        System.out.println("llenarCurso");
        Body instance = new Body();
        String[] expResult = {"1 A","1 B","2 A","2 B"};
        String[] result = instance.llenarCurso();
        assertArrayEquals(expResult, result);
        
        
    }

    /**
     * Test of ArraysOrder method, of class Body.
     * @throws java.lang.Exception
     */
    @Test
    public void testArraysOrder() throws Exception {
        System.out.println("ArraysOrder");
        String[] vectores = {"hola","Test","Mundo","Unitario","JUnit"};
        Body instance = new Body();
        String[] expResult = {"JUnit","Mundo","Test","Unitario","hola"};
        String[] result = instance.ArraysOrder(vectores);
        assertArrayEquals(expResult, result);
        
        
    }
    
}
