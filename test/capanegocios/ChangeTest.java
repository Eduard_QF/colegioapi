/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capanegocios;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import orm.Actividad;
import orm.Apoderado;
import orm.Asignatura;
import orm.Curso;
import orm.Estudiante;
import orm.Institucion;
import orm.Planificacion;
import orm.Profesor;

/**
 *
 * @author Eduard QF
 */
public class ChangeTest {
    
    public ChangeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of changeInstitucion method, of class Change.
     */
    @Test
    public void testChangeInstitucion() throws Exception {
        System.out.println("changeInstitucion");
        String nombreInstitucion = "";
        String newNombre = "";
        boolean expResult = false;
        boolean result = Change.changeInstitucion(nombreInstitucion, newNombre);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of changeCurso method, of class Change.
     */
    @Test
    public void testChangeCurso() throws Exception {
        System.out.println("changeCurso");
        String idCurso = "";
        Institucion institucion = null;
        Institucion newinstitucion = null;
        String newidCurso = "";
        boolean expResult = false;
        boolean result = Change.changeCurso(idCurso, institucion, newinstitucion, newidCurso);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of changeAsignatura method, of class Change.
     */
    @Test
    public void testChangeAsignatura() throws Exception {
        System.out.println("changeAsignatura");
        String nombre = "";
        Curso cur = null;
        String newNombre = "";
        Planificacion newPlan = null;
        Profesor newDocente = null;
        Curso newCurso = null;
        boolean expResult = false;
        boolean result = Change.changeAsignatura(nombre, cur, newNombre, newPlan, newDocente, newCurso);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of changeEstudiante method, of class Change.
     */
    @Test
    public void testChangeEstudiante() throws Exception {
        System.out.println("changeEstudiante");
        String nombre = "";
        String rut = "";
        Apoderado newApoderado = null;
        Curso newCurso = null;
        String newNombre = "";
        String newRut = "";
        int NewFinalAsis = 0;
        boolean expResult = false;
        boolean result = Change.changeEstudiante(nombre, rut, newApoderado, newCurso, newNombre, newRut, NewFinalAsis);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of changeApoderado method, of class Change.
     */
    @Test
    public void testChangeApoderado() throws Exception {
        System.out.println("changeApoderado");
        String nombre = "";
        String rut = "";
        String newNombre = "";
        String newRut = "";
        boolean expResult = false;
        boolean result = Change.changeApoderado(nombre, rut, newNombre, newRut);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of changeEstdiantes_Apoderado method, of class Change.
     */
    @Test
    public void testChangeEstdiantes_Apoderado() throws Exception {
        System.out.println("changeEstdiantes_Apoderado");
        String rut = "";
        Estudiante addEstudiante = null;
        Estudiante removeEstudiante = null;
        boolean expResult = false;
        boolean result = Change.changeEstdiantes_Apoderado(rut, addEstudiante, removeEstudiante);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of changeProfesor method, of class Change.
     */
    @Test
    public void testChangeProfesor() throws Exception {
        System.out.println("changeProfesor");
        String nombre = "";
        String rut = "";
        String newRut = "";
        String newNombre = "";
        boolean expResult = false;
        boolean result = Change.changeProfesor(nombre, rut, newRut, newNombre);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of changePlanificacion method, of class Change.
     */
    @Test
    public void testChangePlanificacion() throws Exception {
        System.out.println("changePlanificacion");
        Asignatura asign = null;
        Asignatura newAsign = null;
        boolean expResult = false;
        boolean result = Change.changePlanificacion(asign, newAsign);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of changeActividad method, of class Change.
     */
    @Test
    public void testChangeActividad() throws Exception {
        System.out.println("changeActividad");
        String fecha = "";
        Planificacion plan = null;
        String newFecha = "";
        String newDetalle = "";
        Planificacion newPlan = null;
        boolean expResult = false;
        boolean result = Change.changeActividad(fecha, plan, newFecha, newDetalle, newPlan);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of changeAsistencia method, of class Change.
     */
    @Test
    public void testChangeAsistencia() throws Exception {
        System.out.println("changeAsistencia");
        String fecha = "";
        Estudiante alumno = null;
        String newFecha = "";
        boolean newAsis = false;
        boolean expResult = false;
        boolean result = Change.changeAsistencia(fecha, alumno, newFecha, newAsis);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of changeAnotacion method, of class Change.
     */
    @Test
    public void testChangeAnotacion() throws Exception {
        System.out.println("changeAnotacion");
        String fecha = "";
        Estudiante alumno = null;
        String newDetalle = "";
        String newFecha = "";
        boolean newTipo = false;
        Profesor newdocente = null;
        boolean expResult = false;
        boolean result = Change.changeAnotacion(fecha, alumno, newDetalle, newFecha, newTipo, newdocente);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of changeNota method, of class Change.
     */
    @Test
    public void testChangeNota() throws Exception {
        System.out.println("changeNota");
        Actividad act = null;
        Estudiante alumno = null;
        float nota = 0.0F;
        boolean expResult = false;
        boolean result = Change.changeNota(act, alumno, nota);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of changePersona method, of class Change.
     */
    @Test
    public void testChangePersona() throws Exception {
        System.out.println("changePersona");
        String nombre = "";
        String rut = "";
        String newNombre = "";
        String newRut = "";
        boolean expResult = false;
        boolean result = Change.changePersona(nombre, rut, newNombre, newRut);
        assertEquals(expResult, result);
        
        
    }
    
}
