/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capanegocios;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import orm.Actividad;
import orm.Anotaciones;
import orm.Apoderado;
import orm.Asignatura;
import orm.Asistencia;
import orm.Curso;
import orm.Estudiante;
import orm.Nota;
import orm.Planificacion;
import orm.Profesor;

/**
 *
 * @author Eduard QF
 */
public class ReadTest {
    
    public ReadTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of readCursos method, of class Read.
     */
    @Test
    public void testReadCursos() throws Exception {
        System.out.println("readCursos");
        Curso[] result = Read.readCursos();
        assertNotNull( result);
        
        
    }

    /**
     * Test of readCurso method, of class Read.
     */
    @Test
    public void testReadCurso() throws Exception {
        System.out.println("readCurso");
        String idCurso = "";
        Curso expResult = null;
        Curso result = Read.readCurso(idCurso);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of readAsignaturas method, of class Read.
     */
    @Test
    public void testReadAsignaturas() throws Exception {
        System.out.println("readAsignaturas");
        String idCurso = "1 A";
        Asignatura[] result = Read.readAsignaturas(idCurso);
        assertNotNull( result);
        
        
    }

    /**
     * Test of readAsignatura method, of class Read.
     */
    @Test
    public void testReadAsignatura() throws Exception {
        System.out.println("readAsignatura");
        String nombreAsign = "Lenguaje";
        String cursoid = "1 A";
        Asignatura result = Read.readAsignatura(nombreAsign, cursoid);
        assertNotNull( result);
        
        
    }

    /**
     * Test of readEstudiantesCurso method, of class Read.
     */
    @Test
    public void testReadEstudiantesCurso() throws Exception {
        System.out.println("readEstudiantesCurso");
        Estudiante[] result = Read.readEstudiantesCurso("1 A");
        assertNotNull( result);
        
        
    }

    /**
     * Test of readPupilos method, of class Read.
     */
    @Test
    public void testReadPupilos() throws Exception {
        System.out.println("readPupilos");
        String nomOrut = "";
        Estudiante[] expResult = null;
        Estudiante[] result = Read.readPupilos(nomOrut);
        assertArrayEquals(expResult, result);
        
        
    }

    /**
     * Test of readEstudiante method, of class Read.
     */
    @Test
    public void testReadEstudiante() throws Exception {
        System.out.println("readEstudiante");
        String nomOrut = "20224628-1";
        Estudiante result = Read.readEstudiante(nomOrut);
        assertNotNull( result);
        
        
    }

    /**
     * Test of readNotasAlumno method, of class Read.
     */
    @Test
    public void testReadNotasAlumno() throws Exception {
        System.out.println("readNotasAlumno");
        String rut = "20224628-1";
        Nota[] result = Read.readNotasAlumno(rut);
        assertNotNull( result);
        
        
    }

    /**
     * Test of readNotasAsignatura method, of class Read.
     */
    @Test
    public void testReadNotasAsignatura() throws Exception {
        System.out.println("readNotasAsignatura");
        String curso = "1 A";
        String asignatura = "Lenguaje";
        Nota[] result = Read.readNotasAsignatura(curso, asignatura);
        assertNull(result);
        
        
    }

    /**
     * Test of readNota method, of class Read.
     */
    @Test
    public void testReadNota() throws Exception {
        System.out.println("readNota");
        Estudiante alumno = null;
        Actividad act = null;
        Nota expResult = null;
        Nota result = Read.readNota(alumno, act);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of readAsistencias method, of class Read.
     */
    @Test
    public void testReadAsistencias() throws Exception {
        System.out.println("readAsistencias");
        String rut = "20224628-1";
        Asistencia[] result = Read.readAsistencias(rut);
        assertNotNull(result);
        
        
    }

    /**
     * Test of readAsistencia method, of class Read.
     */
    @Test
    public void testReadAsistencia() throws Exception {
        System.out.println("readAsistencia");
        String fecha = "";
        Estudiante alumno = null;
        Asistencia expResult = null;
        Asistencia result = Read.readAsistencia(fecha, alumno);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of readAnotaciones method, of class Read.
     */
    @Test
    public void testReadAnotaciones() throws Exception {
        System.out.println("readAnotaciones");
        String rut = "20224628-1";
        Anotaciones[] result = Read.readAnotaciones(rut);
        assertNotNull( result);
        
        
    }

    /**
     * Test of readAnotacion method, of class Read.
     * @throws java.lang.Exception
     */
    @Test
    public void testReadAnotacion() throws Exception {
        System.out.println("readAnotacion");
        String fecha = "";
        String alumno = null;
        Anotaciones expResult = null;
        Anotaciones result = Read.readAnotacion(fecha, alumno);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of readPlanificacion method, of class Read.
     */
    @Test
    public void testReadPlanificacion() throws Exception {
        System.out.println("readPlanificacion");
        Asignatura asign = null;
        Curso curso = null;
        Planificacion expResult = null;
        Planificacion result = Read.readPlanificacion(asign, curso);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of readActividades method, of class Read.
     */
    @Test
    public void testReadActividades() throws Exception {
        System.out.println("readActividades");
        String asign = null;
        String curso=null;
        Actividad[] expResult = null;
        Actividad[] result = Read.readActividades(asign,curso);
        assertArrayEquals(expResult, result);
        
        
    }

    /**
     * Test of readActividad method, of class Read.
     */
    @Test
    public void testReadActividad() throws Exception {
        System.out.println("readActividad");
        String fecha = "";
        Planificacion plan = null;
        Actividad expResult = null;
        Actividad result = Read.readActividad(fecha, plan);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of readProfesoresCurso method, of class Read.
     */
    @Test
    public void testReadProfesoresCurso() throws Exception {
        System.out.println("readProfesoresCurso");
        String idCurso = "1 A";
        
        Profesor[] result = Read.readProfesoresCurso(idCurso);
        assertNotNull( result);
        
        
    }

    /**
     * Test of readProfesor method, of class Read.
     */
    @Test
    public void testReadProfesor() throws Exception {
        System.out.println("readProfesor");
        String nomOrut = "";
        Profesor expResult = null;
        Profesor result = Read.readProfesor(nomOrut);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of readApoderadosCurso method, of class Read.
     */
    @Test
    public void testReadApoderadosCurso() throws Exception {
        System.out.println("readApoderadosCurso");
        String idCurso = "1 A";
        Apoderado[] result = Read.readApoderadosCurso(idCurso);
        assertNotNull( result);
        
        
    }

    /**
     * Test of readApoderado method, of class Read.
     */
    @Test
    public void testReadApoderado() throws Exception {
        System.out.println("readApoderado");
        String rut = "20224628-1";
        Apoderado expResult = null;
        Apoderado result = Read.readApoderado(rut);
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of readComplitApoderado method, of class Read.
     */
    @Test
    public void testReadComplitApoderado() throws Exception {
        System.out.println("readComplitApoderado");
        Apoderado[] result = Read.readComplitApoderado();
        assertNotNull(result);
        
        
    }
    
}
